from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_reader, diplomacy_solve

class TestDiplomacy(TestCase):

    # test diplomacy_reader()
    def test_reader1(self):
        s = ["A SanJose Hold"]
        a = diplomacy_reader(s)
        self.assertEqual(a, [["A", "SanJose", "Hold"]])
        
    def test_reader2(self):
        s = ["B Austin Move SanJose"]
        a = diplomacy_reader(s)
        self.assertEqual(a, [["B", "Austin", "Move", "SanJose"]]) 
        
    def test_reader3(self):
        s = ["C Dallas Support B"]
        a = diplomacy_reader(s)
        self.assertEqual(a, [["C", "Dallas", "Support", "B"]]) 
    
    # test diplomomacy_solve function
    def test_solve1(self):
        r = StringIO("A Boston Hold\nB Fresno Move Houston\nC Houston Support A\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Boston\nB [dead]\nC [dead]\n")

    def test_solve2(self):
        r = StringIO("A SanJose Hold\nB Austin Move SanJose\nC Houston Support B\nD LosAngeles Move Houston\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")    
    
    def test_solve3(self):
        r = StringIO("A SanJose Move Austin\nB Austin Move SanJose\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Austin\nB SanJose\n")      
        
    def test_solve4(self):
        r = StringIO("A Boston Hold\nB Fresno Move Boston\nC Houston Support A\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Boston\nB [dead]\nC Houston\n")    
    
if __name__ == "__main__":  # pragma: no cover
    main()