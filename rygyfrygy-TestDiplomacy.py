
from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve


class TestDiplomacy (TestCase):

    # ------------
    #     read
    # ------------

    def test_read_1(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n"
        i = diplomacy_read(s)
        self.assertEqual(i, [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid']])

    def test_read_2(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Paris Support C\nE Austin Support A\n"
        i = diplomacy_read(s)
        self.assertEqual(i, [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B'], ['D', 'Paris', 'Support', 'C'], ['E', 'Austin', 'Support', 'A']])

    def test_read_3(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Paris Move London\n"
        i = diplomacy_read(s)
        self.assertEqual(i, [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B'], ['D', 'Paris', 'Move', 'London']])


    # ------------
    #     eval
    # ------------

    def test_eval_1(self):
        v = diplomacy_eval([['C', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['A', 'London', 'Move', 'Madrid']])
        self.assertEqual(v, ['A [dead]', 'B [dead]', 'C [dead]'])

    def test_eval_2(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B'], ['D', 'Paris', 'Support', 'C'], ['E', 'Austin', 'Support', 'A']])
        self.assertEqual(v, ['A [dead]', 'B [dead]', 'C London', 'D Paris', 'E Austin'])

    def test_eval_3(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B'], ['D', 'Paris', 'Move', 'London']])
        self.assertEqual(v, ['A [dead]', 'B [dead]', 'C [dead]', 'D [dead]'])

    # ------------
    #    print
    # ------------

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, ['A [dead]', 'B [dead]', 'C [dead]'])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, ['A [dead]', 'B [dead]', 'C London', 'D Paris', 'E Austin'])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC London\nD Paris\nE Austin\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, ['A [dead]', 'B [dead]', 'C [dead]', 'D [dead]'])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    # ------------
    #    solve
    # ------------

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Paris Support C\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC London\nD Paris\nE Austin\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Paris Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

