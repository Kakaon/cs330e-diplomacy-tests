from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_solve


class TestDiplomacy (TestCase):

    def test_read_1(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\n" \
            "C London Move Madrid\nD Paris Support B\n" \
            "E Austin Support A"
        i = diplomacy_read(s)
        self.assertEqual(i, s)

    def test_read_2(self):
        s = "A Minneapolis Hold\n" \
            "B Bangkok Move Minneapolis\n" \
            "C Cairo Support B"
        i = diplomacy_read(s)
        self.assertEqual(i, s)

    def test_read_3(self):
        s = "A Austin Hold\n" \
            "B Bejing Hold\n" \
            "C Calgary Hold"
        i = diplomacy_read(s)
        self.assertEqual(i, s)

    def test_solve_1(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\n" \
            "C London Move Madrid\nD Paris Support B\n" \
            "E Austin Support A"
        w = StringIO()
        i = diplomacy_solve(s, w)
        r = "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin"
        self.assertEqual(i, r)

    def test_solve_2(self):
        s = "A Minneapolis Hold\n" \
            "B Bangkok Move Minneapolis\n" \
            "C Cairo Support B"
        w = StringIO()
        i = diplomacy_solve(s,w)
        r = "A [dead]\nB Minneapolis\nC Cairo"
        self.assertEqual(i, r)

    def test_solve_3(self):
        s = "A Austin Hold\n" \
            "B Bejing Hold\n" \
            "C Calgary Hold\nD Dublin Hold\n" \
            "E Edinburgh Hold\nF Frankfurt Hold"
        w = StringIO()
        i = diplomacy_solve(s,w)
        r = "A Austin\nB Bejing\nC Calgary\nD Dublin\nE Edinburgh\nF Frankfurt"
        self.assertEqual(i, r)

    def test_solve_4(self):
        s = "A Madrid Move London"
        w = StringIO()
        i = diplomacy_solve(s,w)
        r = "A London"
        self.assertEqual(i, r)

    def test_solve_5(self):
        s = "A Austin Move Paris\nB Barcelona Move Paris\nC Tokyo Move Barcelona\nD Seattle Move Tokyo"
        w = StringIO
        i = diplomacy_solve(s, w)
        r = "A [dead]\nB [dead]\nC Barcelona\nD Tokyo"
        self.assertEqual(i, r)
        
    def test_print_1(self):
        w = StringIO()
        teststr1 = "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin"
        diplomacy_print(w, teststr1)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, "A [dead]\nB Minneapolis\nC Cairo")
        self.assertEqual(w.getvalue(), "A [dead]\nB Minneapolis\nC Cairo")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, "A Austin\nB Bejing\nC Calgary\nD Dublin\nE Edinburgh\nF Frankfurt")
        self.assertEqual(w.getvalue(),"A Austin\nB Bejing\nC Calgary\nD Dublin\nE Edinburgh\nF Frankfurt")

if __name__ == "__main__":
    main()

