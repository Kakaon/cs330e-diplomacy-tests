#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold"
        i = diplomacy_read(s)
        self.assertEqual(i,  ["A", "Madrid", "Hold"])
    def test_read_2(self):
        s = "B Barcelona Move Madrid"
        i = diplomacy_read(s)
        self.assertEqual(i,  ["B", "Barcelona", "Move", "Madrid"])
    def test_read_3(self):
        s = "C London Support B"
        i = diplomacy_read(s)
        self.assertEqual(i,  ["C", "London", "Support", "B"])
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"]])
        self.assertEqual(v, [["A", "Madrid"]])
    def test_eval_2(self):
        v = diplomacy_eval([["B", "Barcelona", "Move", "Madrid"]])
        self.assertEqual(v, [["B", "Madrid"]])
    def test_eval_3(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Support", "B"]])
        self.assertEqual(v, [["A", "[dead]"], ["B", "Madrid"], ["C", "London"]])


    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, [["A", "[dead]"], ["B", "[dead]"]])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")
    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, [["A", "Madrid"], ["B", "London"]])
        self.assertEqual(w.getvalue(), "A Madrid\nB London\n")
    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, [["A", "Madrid"], ["B", "London"], ["C", "[dead]"]])
        self.assertEqual(w.getvalue(), "A Madrid\nB London\nC [dead]\n")
    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")
    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out



$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
"""
