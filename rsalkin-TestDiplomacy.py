#!/usr/bin/env python3

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        readTup = diplomacy_read(s)
        self.assertEqual(readTup[0],  "A Madrid Hold")


    def test_read_2(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n"
        readTup = diplomacy_read(s)
        self.assertEqual(readTup,  ("A Madrid Hold",'B Barcelona Move Madrid','C London Support B'))


    def test_read_3(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n"
        readTup = diplomacy_read(s)
        self.assertEqual(readTup,  ('A Madrid Hold','B Barcelona Move Madrid','C London Move Madrid'))

    def test_read_4(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n"
        readTup = diplomacy_read(s)
        self.assertEqual(readTup,  ('A Madrid Hold','B Barcelona Move Madrid','C London Support B','D Austin Move London'))



    # ----
    # eval
    # ----
    def test_eval_1(self):
        v = diplomacy_eval(('A Madrid Hold'))
        self.assertEqual(v, ('A Madrid',))
        
    def test_eval_2(self):
        v = diplomacy_eval(('A Madrid Hold', 'B Barcelona Move Madrid', \
        'C London Support B'))
        self.assertEqual(v, ('A [dead]', 'B Madrid', 'C London'))
        
    def test_eval_3(self):
        v = diplomacy_eval(('A Madrid Hold', 'B Barcelona Move Madrid', \
        'C London Move Madrid', 'D Paris Support B'))
        self.assertEqual(v, ('A [dead]', 'B Madrid', 'C [dead]', 'D Paris'))
        
    def test_eval_4(self):
        v = diplomacy_eval(('A Madrid Hold', 'B Barcelona Move Madrid', \
        'C London Move Madrid', 'D Paris Support B', 'E Austin Support A'))
        self.assertEqual(v, ('A [dead]', 'B [dead]', 'C [dead]', 'D Paris', 'E Austin'))

    def test_eval_5(self):
        with self.assertRaises(Exception) as error_thrown:
            diplomacy_eval((''))
        excpetion_thrown = error_thrown.exception
        self.assertTrue("Invalid Number of Entries for Line" in str(excpetion_thrown))
        
    def test_eval_6(self):
        with self.assertRaises(Exception) as error_thrown:
            diplomacy_eval(('a Madrid Hold', 'B Barcelona Move Madrid'))
        excpetion_thrown = error_thrown.exception
        self.assertTrue("Invalid Key for Army" in str(excpetion_thrown))

    def test_eval_7(self):
        v = diplomacy_eval(('A Madrid Support D', 'B Barcelona Hold', \
        'C London Hold', 'D Paris Support B', 'E Austin Support D'))
        self.assertEqual(v, ('A Madrid', 'B Barcelona', 'C London', 'D Paris', 'E Austin'))

    def test_eval_8(self):
        with self.assertRaises(Exception) as error_thrown:
            diplomacy_eval(('A Madrid Stay', 'B Barcelona Move Madrid'))
        excpetion_thrown = error_thrown.exception
        self.assertTrue("Action Invalid for: A" in str(excpetion_thrown))

    def test_eval_9(self):
        with self.assertRaises(Exception) as error_thrown:
            diplomacy_eval(('A Madrid Support A', 'B Barcelona Move Madrid'))
        excpetion_thrown = error_thrown.exception
        self.assertTrue("Army can't support itself" in str(excpetion_thrown))

    def test_eval_10(self):
        with self.assertRaises(Exception) as error_thrown:
            diplomacy_eval(('A Madrid Support Barcelona', 'B Barcelona Move Madrid'))
        excpetion_thrown = error_thrown.exception
        self.assertTrue("Invalid Support Selection: Barcelona" in str(excpetion_thrown))
        
    '''def test_eval_1(self):
        v = diplomacy_eval(('A Madrid Hodl'))
        self.assertEqual(v, ('A Madrid',))'''



    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, ('A Madrid'))
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, ('A [dead]', 'B Madrid', 'C London'))
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, ('A [dead]','B [dead]','C [dead]','D Paris','E Austin'))
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    # -----
    # solve
    # -----
    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")
            
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")
            
    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
        
    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

# -----
# main
# -----

if __name__ == "__main__": #pragma no cover
    main()
