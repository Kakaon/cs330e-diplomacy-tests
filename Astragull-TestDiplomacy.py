#!/usr/bin/env python3

# -------
# imports
# -------
from io import StringIO
from unittest import main, TestCase
from Diplomacy import Army, diplomacy_read, diplomacy_solve, diplomacy_move, diplomacy_support_check, diplomacy_resolve

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy(TestCase):


    # -----------
    # solve
    # -----------

    def test_diplomacy_solve(self):
        r, w = StringIO("A Dallas Hold\nB Austin Move Dallas\nC Houston Support B\n"), StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Dallas\nC Houston\n")

    def test_diplomacy_solve2(self):
        r, w = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n"), StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_diplomacy_solve3(self):
        r, w = StringIO("A NewYork Move Miami\nB Miami Hold\nC Seattle Support B\nD SanFrancisco Support B\nE Denton Move Seattle\n"), StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Miami\nC [dead]\nD SanFrancisco\nE [dead]\n")
    


    # -----------
    # read
    # -----------

    def test_diplomacy_read(self):
        r = StringIO("A Madrid Hold\n")
        scenario, armies = diplomacy_read(r)
        self.assertEqual(scenario, [['A', 'Madrid', 'Hold']])
        self.assertEqual(armies[0].name, 'A')
        self.assertEqual(armies[0].city, 'Madrid')


    def test_diplomacy_read2(self):
        r = StringIO("B Barcelona Move Madrid\n")
        scenario, armies = diplomacy_read(r)
        self.assertEqual(scenario, [['B', 'Barcelona', 'Move', 'Madrid']])
        self.assertEqual(armies[0].name, 'B')
        self.assertEqual(armies[0].city, 'Barcelona')


    def test_diplomacy_read3(self):
        r = StringIO("C London Move Madrid\n")
        scenario, armies = diplomacy_read(r)
        self.assertEqual(scenario, [['C', 'London', 'Move', 'Madrid']])
        self.assertEqual(armies[0].name, 'C')
        self.assertEqual(armies[0].city, 'London')

    # -----------
    # move
    # -----------

    def test_diplomacy_move(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        scenario, armies = diplomacy_read(r)
        armies, scenario, cities = diplomacy_move(armies, scenario, cities = {})
        
        self.assertEqual(armies[0].city, "Madrid")
        self.assertEqual(armies[1].city, "Madrid")
        self.assertEqual(armies[2].city, "Madrid")
        self.assertDictEqual(cities, {"Madrid": 3})

    def test_diplomacy_move2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Hold\nD Tokyo Move London")
        scenario, armies = diplomacy_read(r)
        armies, scenario, cities = diplomacy_move(armies, scenario, cities = {})
        
        self.assertEqual(armies[0].city, "Madrid")
        self.assertEqual(armies[1].city, "Madrid")
        self.assertEqual(armies[2].city, "London")
        self.assertEqual(armies[3].city, "London")
        self.assertDictEqual(cities, {"Madrid": 2, "London": 2})


    def test_diplomacy_move3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Tokyo Move London")
        scenario, armies = diplomacy_read(r)
        armies, scenario, cities = diplomacy_move(armies, scenario, cities = {})
        self.assertEqual(armies[0].city, "Madrid")
        self.assertEqual(armies[1].city, "Madrid")
        self.assertEqual(armies[2].city, "Madrid")
        self.assertEqual(armies[3].city, "London")
        self.assertDictEqual(cities, {"Madrid": 3, "London": 1})

    # -----------
    # support_check
    # -----------
    def test_support_check(self):
        r = StringIO("A Madrid Hold\nB Barcelona Support A\nC London Move Barcelona\n")
        scenario, armies = diplomacy_read(r)
        armies, scenario, cities = diplomacy_move(armies, scenario, cities = {})
        diplomacy_support_check(armies, cities)
        self.assertEqual(armies[0].supportCount, 0)
        self.assertEqual(armies[1].supportCount, 0)
        self.assertEqual(armies[2].supportCount, 0)
        
    def test_support_check2(self):
        r = StringIO("A Houston Support B\nB Amsterdam Hold\nC Paris Support B\n")
        scenario, armies = diplomacy_read(r)
        armies, scenario, cities = diplomacy_move(armies, scenario, cities = {})
        diplomacy_support_check(armies, cities)
        self.assertEqual(armies[0].supportCount, 0)
        self.assertEqual(armies[1].supportCount, 2)
        self.assertEqual(armies[2].supportCount, 0)
    
    def test_support_check3(self):
        r = StringIO("A NewYork Move Miami\nB Miami Hold\nC Seattle Support B\nD SanFrancisco Support B\nE Denton Move Seattle\n")
        scenario, armies = diplomacy_read(r)
        armies, scenario, cities = diplomacy_move(armies, scenario, cities = {})
        diplomacy_support_check(armies, cities)
        self.assertEqual(armies[0].supportCount, 0)
        self.assertEqual(armies[1].supportCount, 1)
        self.assertEqual(armies[2].supportCount, 0)
        self.assertEqual(armies[3].supportCount, 0)
        self.assertEqual(armies[4].supportCount, 0)


    # -----------
    # resolve
    # -----------

    def test_resolve(self):
        r = StringIO("A NewYork Move Miami\nB Miami Hold\nC Seattle Support B\nD SanFrancisco Support B\nE Denton Move Seattle\n")
        scenario, armies = diplomacy_read(r)
        armies, scenario, cities = diplomacy_move(armies, scenario, cities = {})
        diplomacy_support_check(armies, cities)
        diplomacy_resolve(armies, cities)
        self.assertEqual(armies[0].status, 0)
        self.assertEqual(armies[1].status, 1)
        self.assertEqual(armies[2].status, 0)
        self.assertEqual(armies[3].status, 1)
        self.assertEqual(armies[4].status, 0)

    def test_resolve2(self):
        r = StringIO("A Houston Hold\nB Amsterdam Hold\nC Paris Hold\n")
        scenario, armies = diplomacy_read(r)
        armies, scenario, cities = diplomacy_move(armies, scenario, cities = {})
        diplomacy_support_check(armies, cities)
        diplomacy_resolve(armies, cities)
        self.assertEqual(armies[0].status, 1)
        self.assertEqual(armies[1].status, 1)
        self.assertEqual(armies[2].status, 1)

    def test_resolve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Support A\nC London Move Barcelona\nD Chicago Move Madrid\n")
        scenario, armies = diplomacy_read(r)
        armies, scenario, cities = diplomacy_move(armies, scenario, cities = {})
        diplomacy_support_check(armies, cities)
        diplomacy_resolve(armies, cities)
        self.assertEqual(armies[0].status, 0)
        self.assertEqual(armies[1].status, 0)
        self.assertEqual(armies[2].status, 0)
        self.assertEqual(armies[3].status, 0)

  

if __name__ == "__main__": #pragma: no cover
    main()

