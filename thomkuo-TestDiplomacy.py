#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from tokenize import String
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # solve
    # ----

    def test_solve_1(self):
        r = StringIO("A Dallas Hold\nB Austin Move Dallas")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_2(self):
        r = StringIO("A Austin Hold\nB NewYork Move Austin\nC LosAngeles Support B\nD Shanghai Move LosAngeles")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Dallas Hold\nB Austin Hold")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Dallas\nB Austin\n")

    def test_solve_4(self):
        r = StringIO("A NewYork Move Berlin\nB Berlin Hold")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")
    
    def test_solve_5(self):
        r = StringIO("A Houston Support B\nB Dallas Support C\nC NewOrleans Hold\nD Lincoln Move Dallas\nE Austin Move NewOrleans \nF SanAntonio Support E")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Houston\nB Dallas\nC [dead]\nD [dead]\nE NewOrleans\nF SanAntonio\n")

    def test_solve_6(self):
        r = StringIO("A Frisco Support B\nB Allen Move Southlake\nC Southlake Hold")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Frisco\nB Southlake\nC [dead]\n")

    def test_solve_7(self):
        r = StringIO("A Tokyo Move LosAngeles\nB Portland Hold\nC LosAngeles Support B")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Portland\nC [dead]\n")

# ----
# main
# ----

if __name__ == "__main__": 
    main()

""" #pragma: no cover

"""