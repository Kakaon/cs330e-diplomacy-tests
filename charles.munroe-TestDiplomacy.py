
from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_solve, diplomacy_print, data_change, whos_in_the_city, support_invalidator


class TestDiplomacy(TestCase):

    def test_read_0(self):
        s = "A Boston Move ElPaso"
        string = diplomacy_read(s)
        self.assertEqual(string,['A', 'Boston', 'Move', 'ElPaso'])

    def test_read_1(self):
        s = "B Belton Hold"
        string = diplomacy_read(s)
        self.assertEqual(string,['B', 'Belton', 'Hold'])

    def test_read_2(self):
        s = "C Temple Support A"
        string = diplomacy_read(s)
        self.assertEqual(string,['C', 'Temple', 'Support', 'A'])

    def test_data_change_0(self):
        ltwod = [['A', 'Boston', 'Move', 'ElPaso'],['B', 'Temple', 'Move', 'ElPaso'],["C", 'Belton', 'Support', 'A'],['D', 'Killeen', 'Move', 'Belton']]
        output = data_change(ltwod)
        self.assertEqual(output, {"A":{'Position':'ElPaso', 'Support':["C"]}, 'B':{'Position':'ElPaso', 'Support':[]}, 'C':{'Position': 'Belton', 'Support':[]}, 'D':{'Position': 'Belton', 'Support':[]}})

    def test_data_change_1(self):
        ltwod = [['A', 'NewYork', 'Hold'], ['B', 'NewBraunfels', 'Move', 'NewYork'], ['C', 'Austin', 'Support', 'A'], ['D','CollegeStation', 'Support', 'A']]
        output = data_change(ltwod)
        self.assertEqual(output, {"A":{'Position':'NewYork', 'Support':["C","D"]}, 'B':{'Position':'NewYork', 'Support':[]}, 'C':{'Position': 'Austin', 'Support':[]}, 'D':{'Position': 'CollegeStation', 'Support':[]}})

    def test_data_change_2(self):
        ltwod = [['A', 'NewYork', 'Hold'], ['B', 'NewBraunfels', 'Support', 'A'], ['C', 'Austin', 'Support', 'A'], ['D','CollegeStation', 'Support', 'A']]
        output = data_change(ltwod)
        self.assertEqual(output, {"A":{'Position':'NewYork', 'Support':["B","C","D"]}, 'B':{'Position':'NewBraunfels', 'Support':[]}, 'C':{'Position': 'Austin', 'Support':[]}, 'D':{'Position': 'CollegeStation', 'Support':[]}})

    def test_whos_in_the_city_0(self):
        data_mod = {"A":{'Position':'ElPaso', 'Support':["C"]}, 'B':{'Position':'ElPaso', 'Support':[]}, 'C':{'Position': 'Belton', 'Support':[]}, 'D':{'Position': 'Belton', 'Support':[]}}
        output = whos_in_the_city(data_mod)
        self.assertEqual(output, {"ElPaso":['A','B'], "Belton":['C','D']})

    def test_whos_in_the_city_1(self):
        data_mod = {"A":{'Position':'ElPaso', 'Support':["C"]}, 'B':{'Position':'ElPaso', 'Support':[]}, 'C':{'Position': 'ElPaso', 'Support':[]}, 'D':{'Position': 'ElPaso', 'Support':[]}}
        output = whos_in_the_city(data_mod)
        self.assertEqual(output, {"ElPaso":['A','B','C','D']})
    
    def test_whos_in_the_city_2(self):
        data_mod = {"A":{'Position':'ElPaso', 'Support':["C"]}, 'B':{'Position':'ElPaso', 'Support':[]}, 'C':{'Position': 'Belton', 'Support':[]}, 'D':{'Position': 'Belton', 'Support':[]}, 'E':{"Position": "Belton", "Support":[]}}
        output = whos_in_the_city(data_mod)
        self.assertEqual(output, {"ElPaso":['A','B'], "Belton":['C','D','E']})

    def test_support_invalidator_0(self):
        cities_dict = {"ElPaso":['A','B'], "Belton":['C','D']}
        armies_dict = {"A":{'Position':'ElPaso', 'Support':["C"]}, 'B':{'Position':'ElPaso', 'Support':[]}, 'C':{'Position': 'Belton', 'Support':[]}, 'D':{'Position': 'Belton', 'Support':[]}}
        output = support_invalidator(cities_dict,armies_dict)
        self.assertEqual(output, {"A":{'Position':'ElPaso', 'Support':[]}, 'B':{'Position':'ElPaso', 'Support':[]}, 'C':{'Position': 'Belton', 'Support':[]}, 'D':{'Position': 'Belton', 'Support':[]}})
    
    #this is the one which formerly had a bug, was fixed by changing the type of a variable to a list so that it is iterated through properly
    def test_support_invalidator_1(self):
        cities_dict = {"ElPaso":['A'], 'Temple':['B','E'], "Belton":['C','D']}
        armies_dict = {"A":{'Position':'ElPaso', 'Support':["C","B"]}, 'B':{'Position':'Temple', 'Support':[]}, 'C':{'Position': 'Belton', 'Support':[]}, 'D':{'Position': 'Belton', 'Support':[]}, "E":{'Position':'Temple', 'Support':[]}}
        output = support_invalidator(cities_dict,armies_dict)
        self.assertEqual(output, {"A":{'Position':'ElPaso', 'Support':[]}, 'B':{'Position':'Temple', 'Support':[]}, 'C':{'Position': 'Belton', 'Support':[]}, 'D':{'Position': 'Belton', 'Support':[]}, "E":{'Position':'Temple', 'Support':[]}})
    
    def test_support_invalidator_2(self):
        cities_dict = {"ElPaso":['A'], "Temple":["B"], "Belton":['C'], "Killeen":["D"]}
        armies_dict = {"A":{'Position':'ElPaso', 'Support':["B","C","D"]}, 'B':{'Position':'Temple', 'Support':[]}, 'C':{'Position': 'Belton', 'Support':[]}, 'D':{'Position': 'Killeen', 'Support':[]}}
        output = support_invalidator(cities_dict,armies_dict)
        self.assertEqual(output, {"A":{'Position':'ElPaso', 'Support':["B","C","D"]}, 'B':{'Position':'Temple', 'Support':[]}, 'C':{'Position': 'Belton', 'Support':[]}, 'D':{'Position': 'Killeen', 'Support':[]}})

    def test_eval_0(self):
        armies_dict = {"A":{'Position':'ElPaso', 'Support':[]}, 'B':{'Position':'ElPaso', 'Support':[]}, 'C':{'Position': 'Belton', 'Support':[]}, 'D':{'Position': 'Belton', 'Support':[]}}
        cities_dict = {"ElPaso":['A','B'], "Belton":['C','D']}
        output = diplomacy_eval(cities_dict,armies_dict)
        self.assertEqual(output, {"A":{'Position':'[dead]', 'Support':[]}, 'B':{'Position':'[dead]', 'Support':[]}, 'C':{'Position': '[dead]', 'Support':[]}, 'D':{'Position': '[dead]', 'Support':[]}})

    def test_eval_1(self):
        armies_dict = {"A":{'Position':'ElPaso', 'Support':[]}, 'B':{'Position':'Temple', 'Support':[]}, 'C':{'Position': 'Belton', 'Support':[]}, 'D':{'Position': 'Killeen', 'Support':[]}}
        cities_dict = {"ElPaso":['A'], "Temple":["B"], "Belton":['C'], "Killeen":["D"]}
        output = diplomacy_eval(cities_dict,armies_dict)
        self.assertEqual(output, {"A":{'Position':'ElPaso', 'Support':[]}, 'B':{'Position':'Temple', 'Support':[]}, 'C':{'Position': 'Belton', 'Support':[]}, 'D':{'Position': 'Killeen', 'Support':[]}})

    def test_eval_2(self):
        armies_dict = {"A":{'Position':'ElPaso', 'Support':[]}, 'B':{'Position':'ElPaso', 'Support':[]}, 'C':{'Position': 'Belton', 'Support':[]}, 'D':{'Position': 'Killeen', 'Support':[]}}
        cities_dict = {"ElPaso":['A','B'], "Belton":['C'], "Killeen":['D']}
        output = diplomacy_eval(cities_dict,armies_dict)
        self.assertEqual(output, {"A":{'Position':'[dead]', 'Support':[]}, 'B':{'Position':'[dead]', 'Support':[]}, 'C':{'Position': 'Belton', 'Support':[]}, 'D':{'Position': 'Killeen', 'Support':[]}})

    def test_solve_0(self):
        r = StringIO("A Austin Hold\nB Dallas Hold\nC Houston Move Toronto\n") #possible input
        w = StringIO() #output write
        diplomacy_solve(r, w) #calls solve and gives it the read and write
        self.assertEqual(w.getvalue(), "A Austin\nB Dallas\nC Toronto\n") #checks if correct

    def test_solve_1(self):
        r = StringIO("A Murphy Move Hyderabad\nB Richardson Move Hyderabad\nC Allen Support A\nD London Move Paris\nE Paris Hold\nF Waco Move Hyderabad\n") #possible input
        w = StringIO() #output write
        diplomacy_solve(r, w) #calls the function
        self.assertEqual(w.getvalue(), "A Hyderabad\nB [dead]\nC Allen\nD [dead]\nE [dead]\nF [dead]\n") #checks if correct

    def test_solve_2(self):
        r = StringIO("A Austin Hold\nB Dallas Hold\nC Houston Move Toronto\n") #possible input from before
        w = StringIO() #output write
        diplomacy_solve(r, w) #calls the function
        s = w.getvalue() 
        s = s.replace(" ", "") #splits into list
        for i in s.split(): #checks each item
            self.assertEqual(isinstance(i, str), True) #make sure all parts of input are string, as expected

    def test_print_0(self):
        w = StringIO()
        diplomacy_print(w, "A", "Houston")
        self.assertEqual(w.getvalue(), "A Houston\n")

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, "Z", "[dead]")
        self.assertEqual(w.getvalue(), "Z [dead]\n")

# confirms that the values in the string are infact digits
    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, "H", "[dead]")
        s = w.getvalue()
        s = s.replace(" ", "")
        for i in s.split():
            self.assertEqual(isinstance(i, str), True)

if __name__ == "__main__":
    main()