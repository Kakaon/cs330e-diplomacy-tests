# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve, Army

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        a = diplomacy_read(s)
        self.assertIsInstance(a, Army)
        self.assertEqual(a.name, "A")
        self.assertEqual(a.loc, "Madrid")
        self.assertEqual(a.action, "Hold")

    def test_read_2(self):
        s = "B Barcelona Move Madrid\n"
        a = diplomacy_read(s)
        self.assertIsInstance(a, Army)
        self.assertEqual(a.name, "B")
        self.assertEqual(a.loc, "Barcelona")
        self.assertEqual(a.action, "Move")
        self.assertEqual(a.receiver, "Madrid")

    def test_read_3(self):
        s = "C London Support B\n"
        a = diplomacy_read(s)
        self.assertIsInstance(a, Army)
        self.assertEqual(a.name, "C")
        self.assertEqual(a.loc, "London")
        self.assertEqual(a.action, "Support")
        self.assertEqual(a.receiver, "B")

    # def test_read_4(self):
        # s = "London C B Support\n"
        # self.assertEqual(diplomacy_read(s), AssertionError)
        

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval([Army("A", "Madrid", "Hold")])
        self.assertEqual(v, [Army("A", "Madrid", "Hold", None, 1, 0, True)])

    def test_eval_2(self):
        v = diplomacy_eval([Army("A", "Madrid", "Hold"), Army("B", "Barcelona", "Move", "Madrid"), Army("C", "London", "Support", "B")])
        self.assertEqual(v, [Army("A", "Madrid", "Hold", None, 1, 2, False), Army("B", "Madrid", "Move", "Madrid", 2, 1, True), Army("C", "London", "Support", "B", 1, 0, True)])
        #print("Test Eval 2 worked!-------------------------------------------------------!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

    def test_eval_3(self):
        v = diplomacy_eval([Army("A", "Madrid", "Hold"), Army("B", "Barcelona", "Move", "Madrid")])
        self.assertEqual(v, [Army("A", "Madrid", "Hold", None, 1, 1, False), Army("B", "Madrid", "Move", "Madrid", 1, 1, False)])

    def test_eval_4(self):
        v = diplomacy_eval([Army("A", "Madrid", "Hold"), Army("B", "Barcelona", "Move", "Madrid"), Army("C", "London", "Support", "B"), Army("D", "Austin", "Move", "London")])
        self.assertEqual(v, [Army("A", "Madrid", "Hold", None, 1, 1, False), Army("B", "Madrid", "Move", "Madrid", 1, 1, False), Army("C", "London", "Support", "B", 1, 1, False), Army("D", "London", "Move", "London", 1, 1, False)])

    def test_eval_5(self):
        i = [Army("A", "Madrid", "Hold"), Army("B", "Barcelona", "Move", "Madrid"), Army("C", "London", "Move", "Madrid")]
        o = [Army("A", "Madrid", "Hold", None, 1, 2, False), Army("B", "Madrid", "Move", "Madrid", 1, 2, False), Army("C", "Madrid", "Move", "Madrid", 1, 2, False)]
        v = diplomacy_eval(i)
        # for a in i:
            # print(str(a.__dict__))
        # for a in o:
            # print(str(a.__dict__))
        self.assertEqual(v, o)

    def test_eval_6(self):
        i = [Army("A", "Madrid", "Hold"), Army("B", "Barcelona", "Move", "Madrid"), Army("C", "London", "Move", "Madrid"), Army("D", "Paris", "Support", "B")]
        o = [Army("A", "Madrid", "Hold", None, 1, 3, False), Army("B", "Madrid", "Move", "Madrid", 2, 2, True), Army("C", "Madrid", "Move", "Madrid", 1, 3, False), Army("D", "Paris", "Support", "B", 1, 0, True)]
        v = diplomacy_eval(i)
        # for a in i:
            # print(str(a.__dict__))
        # for a in o:
            # print(str(a.__dict__))
        self.assertEqual(v, o)

    def test_eval_7(self):
        i = [Army("A", "Madrid", "Hold"), Army("B", "Barcelona", "Move", "Madrid"), Army("C", "London", "Move", "Madrid"), Army("D", "Paris", "Support", "B"), Army("E", "Austin", "Support", "A")]
        o = [Army("A", "Madrid", "Hold", None, 2, 3, False), Army("B", "Madrid", "Move", "Madrid", 2, 3, False), Army("C", "Madrid", "Move", "Madrid", 1, 4, False), Army("D", "Paris", "Support", "B", 1, 0, True), Army("E", "Austin", "Support", "A", 1, 0, True)]
        v = diplomacy_eval(i)
        # for a in i:
            # print(str(a.__dict__))
        # for a in o:
            # print(str(a.__dict__))
        self.assertEqual(v, o)


    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, [Army("A", "Madrid", "Hold")])
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, [Army("A", "Madrid", "Hold", lived = False), Army("B", "Madrid", "Move", "Madrid"), Army("C", "London", "Support", "B")])
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, [Army("A", "Madrid", "Hold", lived = False), Army("B", "Barcelona", "Move", "Madrid", lived = False)])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()
    
""" #pragma: no cover
coverage run    --branch TestDiplomacy.py >  TestDiplomacy.tmp 2>&1
coverage report -m                      >> TestDiplomacy.tmp
cat TestDiplomacy.tmp
................
----------------------------------------------------------------------
Ran 16 tests in 0.002s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          84      0     52      0   100%
TestDiplomacy.py      83      0      2      0   100%
--------------------------------------------------------------
TOTAL                167      0     54      0   100%
"""